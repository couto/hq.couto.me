# hq.couto.me

## Current State

| hostname     | ip       | services                 | hardware              | description |
|:-------------|----------|--------------------------|-----------------------|-------------|
| sun.         | 10.0.0.1 | Router                   | Apple AirPort Extreme |             |
| moon.        | 10.0.0.7 | OpenVPN                  | Raspberry Pi 3 1Gb    |             |
| phobos.      | 10.0.0.2 | DNS/DHCP/PiHole/ddclient | Raspberry Pi 1 256Mb  |             |
| deimos.      | 10.0.0.4 | ZNC/Bitlbee              | Raspberry Pi 1 512Mb  |             |
| io.          | 10.0.0.5 | NextCloud                | Raspberry Pi 3 1Gb    | Offline     |
| europa.      | 10.0.0.6 |                          | Raspberry Pi 1 512Mb  |             |
| ganymede.    | 10.0.0.8 |                          | Raspberry Pi 1 512Mb  |             |
| callisto.    | 10.0.0.9 |                          | Raspberry Pi 1 512Mb  |             |
| diskstation. | 10.0.0.3 | NAS                      | Synology DS1515+      |             |

## Proposed State

| hostname     | ip       | services                 | hardware              | description   |
|:-------------|----------|--------------------------|-----------------------|---------------|
| sun.         | 10.0.0.1 | Router                   | Apple AirPort Extreme |               |
| moon.        | 10.0.1.2 | DNS/DHCP/PiHole/ddclient | Raspberry Pi 1 256Mb  |               |
| phobos.      | 10.0.1.3 | OpenVPN/Wireguard/stunnel| Raspberry Pi 3 1Gb    |               |
| deimos.      | 10.0.1.4 | NextCloud/Postgres/Redis | Raspberry Pi 3 1Gb    |               |
| io.          | 10.0.1.5 |                          | Raspberry Pi 1 512Mb  | Swarm Manager |
| europa.      | 10.0.1.6 |                          | Raspberry Pi 1 512Mb  | Swarm Host    |
| ganymede.    | 10.0.1.7 |                          | Raspberry Pi 1 512Mb  | Swarm Host    |
| callisto.    | 10.0.1.8 |                          | Raspberry Pi 1 512Mb  | Swarm Host    |
| diskstation. | 10.0.2.1 | NAS                      | Synology DS1515+      |               |

# Required Services

- [Restic](https://restic.net/)
- [ZNC](https://wiki.znc.in/ZNC)
- [Bitlbee](https://bitlbee.org/)

# Useful Links

- [ARM Docker images](https://hub.docker.com/u/arm32v6)